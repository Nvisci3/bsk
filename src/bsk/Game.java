package bsk;

public class Game {

	/**
	 * It initializes an empty bowling game.
	 */
	
	private int totalScore = 0;
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;
	private int countStrike = 0;

	Frame frame1;
	Frame frame2;
	Frame frame3;
	Frame frame4;
	Frame frame5;
	Frame frame6;
	Frame frame7;
	Frame frame8;
	Frame frame9;
	Frame frame10;
	int count = 0;
	
	public Game() {
		frame1 = null;
		frame2 = null;
		frame3 = null;
		frame4 = null;
		frame5 = null;
		frame6 = null;
		frame7 = null;
		frame8 = null;
		frame9 = null;
		frame10 = null;
		count = 0;
		totalScore = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		count++;
		if(count == 1) {
			frame1 = frame;
		}else if(count == 2) {
			frame2 = frame;
		}else if(count == 3) {
			frame3 = frame;
		}else if(count == 4) {
			frame4 = frame;
		}else if(count == 5) {
			frame5 = frame;
		}else if(count == 6) {
			frame6 = frame;
		}else if(count == 7) {
			frame7 = frame;
		}else if(count == 8) {
			frame8 = frame;
		}else if(count == 9) {
			frame9 = frame;
		}else if(count == 10) {
			frame10 = frame;
		}
				
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		if(index == 0) {
			return frame1;
		}else if(index == 1) {
			return frame2;
		}else if(index == 2) {
			return frame3;
		}else if(index == 3) {
			return frame4;
		}else if(index == 4) {
			return frame5;
		}else if(index == 5) {
			return frame6;
		}else if(index == 6) {
			return frame7;
		}else if(index == 7) {
			return frame8;
		}else if(index == 8) {
			return frame9;
		}else if(index == 9) {
			return frame10;
		}
		return null;
		
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		countStrike = 0;
		
		if(frame1.isStrike() && frame2.isStrike()) {
			countStrike++;
			frame1.setBonus(frame2.getFirstThrow() + frame3.getFirstThrow());
		}else if(frame1.isStrike() && !frame2.isStrike()) {
			frame1.setBonus(frame2.getFirstThrow() + frame2.getSecondThrow());
		}else if(frame1.isSpare()) {
			frame1.setBonus(frame2.getFirstThrow());
		}
		
		if(frame2.isStrike() && frame3.isStrike()) {
			countStrike++;
			frame2.setBonus(frame3.getFirstThrow() + frame4.getFirstThrow());
		}else if(frame2.isStrike() && !frame3.isStrike()) {
			frame2.setBonus(frame3.getFirstThrow() + frame3.getSecondThrow());
		}else if(frame2.isSpare()) {
			frame2.setBonus(frame3.getFirstThrow());
		}
		
		if(frame3.isStrike() && frame4.isStrike()) {
			countStrike++;
			frame3.setBonus(frame4.getFirstThrow() + frame5.getFirstThrow());
		}else if(frame3.isStrike() && !frame4.isStrike()) {
			frame3.setBonus(frame4.getFirstThrow() + frame4.getSecondThrow());
		}else if(frame3.isSpare()) {
			frame3.setBonus(frame4.getFirstThrow());
		}
		
		if(frame4.isStrike() && frame5.isStrike()) {
			countStrike++;
			frame4.setBonus(frame5.getFirstThrow() + frame6.getFirstThrow());
		}else if(frame4.isStrike() && !frame5.isStrike()) {
			frame4.setBonus(frame5.getFirstThrow() + frame5.getSecondThrow());
		}else if(frame4.isSpare()) {
			frame4.setBonus(frame5.getFirstThrow());
		}
		
		if(frame5.isStrike() && frame6.isStrike()) {
			countStrike++;
			frame5.setBonus(frame6.getFirstThrow() + frame7.getFirstThrow());
		}else if(frame5.isStrike() && !frame6.isStrike()) {
			frame5.setBonus(frame6.getFirstThrow() + frame6.getSecondThrow());
		}else if(frame5.isSpare()) {
			frame5.setBonus(frame6.getFirstThrow());
		}
		
		if(frame6.isStrike() && frame7.isStrike()) {
			countStrike++;
			frame6.setBonus(frame7.getFirstThrow() + frame8.getFirstThrow());
		}else if(frame6.isStrike() && !frame7.isStrike()) {
			frame6.setBonus(frame7.getFirstThrow() + frame7.getSecondThrow());
		}else if(frame6.isSpare()) {
			frame6.setBonus(frame7.getFirstThrow());
		}
		
		if(frame7.isStrike() && frame8.isStrike()) {
			countStrike++;
			frame7.setBonus(frame8.getFirstThrow() + frame9.getFirstThrow());
		}else if(frame7.isStrike() && !frame8.isStrike()) {
			frame7.setBonus(frame8.getFirstThrow() + frame8.getSecondThrow());
		}else if(frame7.isSpare()) {
			frame7.setBonus(frame8.getFirstThrow());
		}
		
		if(frame8.isStrike() && frame9.isStrike()) {
			countStrike++;
			frame8.setBonus(frame9.getFirstThrow() + frame10.getFirstThrow());
		}else if(frame8.isStrike() && !frame9.isStrike()) {
			frame8.setBonus(frame9.getFirstThrow() + frame9.getSecondThrow());
		}else if(frame8.isSpare()) {
			frame8.setBonus(frame9.getFirstThrow());
		}
		
		if(frame9.isStrike() && frame10.isStrike() && countStrike == 8) {
			countStrike++;
			firstBonusThrow = 10;
			setFirstBonusThrow(firstBonusThrow);
			totalScore += frame9.getFirstThrow() + frame10.getFirstThrow() + getFirstBonusThrow();
		}else if(frame9.isStrike()) {
			frame9.setBonus(frame10.getFirstThrow() + frame10.getSecondThrow());
			totalScore += frame9.getFirstThrow() + frame9.getSecondThrow() + frame9.getBonus();
		}else if(frame9.isSpare()) {
			frame9.setBonus(frame10.getFirstThrow());
			totalScore += frame9.getFirstThrow() + frame9.getSecondThrow() + frame9.getBonus();
		}else {
			totalScore += frame9.getFirstThrow() + frame9.getSecondThrow();
		}
		
		if(frame10.isStrike() && countStrike == 9) {
			firstBonusThrow = 10;
			secondBonusThrow = 10;
			setFirstBonusThrow(firstBonusThrow);
			setSecondBonusThrow(secondBonusThrow);
			totalScore += frame10.getFirstThrow() + frame10.getSecondThrow() + getFirstBonusThrow() + getSecondBonusThrow();
		}else if(frame10.isStrike()) {
			firstBonusThrow = 7;
			secondBonusThrow = 2;
			setFirstBonusThrow(firstBonusThrow);
			setSecondBonusThrow(secondBonusThrow);
			totalScore += frame10.getFirstThrow() + frame10.getSecondThrow() + getFirstBonusThrow() + getSecondBonusThrow();
		}else if(frame10.isSpare()) {
			firstBonusThrow = 7;
			setFirstBonusThrow(firstBonusThrow);
			totalScore += frame10.getFirstThrow() + frame10.getSecondThrow() + getFirstBonusThrow();
		}else{
			totalScore += frame10.getFirstThrow() + frame10.getSecondThrow();
		}
		
		totalScore += frame1.getScore();				
		totalScore += frame2.getScore();
		totalScore += frame3.getScore();
		totalScore += frame4.getScore();
		totalScore += frame5.getScore();
		totalScore += frame6.getScore();
		totalScore += frame7.getScore();
		totalScore += frame8.getScore();
		
		return totalScore;	
	}

	

}
