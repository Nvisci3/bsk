package bsk;

public class Frame {

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	
	private int firstThrow = 0;
	private int secondThrow = 0;
	private int bonus = 0;
	
	public Frame(int inputFirstThrow, int inputSecondThrow) throws BowlingException {
		firstThrow = inputFirstThrow;
		secondThrow = inputSecondThrow;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		
		return firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {

		return secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		int score = 0;
		if(isStrike() == true) {
			score = firstThrow + secondThrow + getBonus();
			return score;
		}else if(isSpare() == true) {
			score = firstThrow + secondThrow + getBonus();
	        return score;
		}else return firstThrow + secondThrow;	
		
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		if(firstThrow == 10 && secondThrow == 0) {
			return true;
		}
		return false;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		if((firstThrow + secondThrow) == 10) {
			return true;
		}
		return false;		
	}

}
