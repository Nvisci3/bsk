package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import bsk.Frame;
import bsk.Game;

public class GameTest {

	@Test
	public void testGetFrameAt() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(1,5);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(frame5,game.getFrameAt(4));
		
		
	}
	
	@Test
	public void testGameCalculateScore() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(1,5);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(81,game.calculateScore());
		
		
	}
	
	@Test
	public void testSpareFrame() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(1,9);  //Spare
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);  
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(88,game.calculateScore());
		
		
	}
	
	@Test
	public void testStrikeFrame() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(10,0);  //Strike
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);  
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(94,game.calculateScore());
		
		
	}
	
	@Test
	public void testStrikeAndSpareFrame() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(10,0);  //Strike
		Frame frame2 = new Frame(4,6);   //Spare
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);  
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(103,game.calculateScore());
		
		
	}
	
	@Test
	public void testMultipleStrikesFrame() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(10,0);  //Strike
		Frame frame2 = new Frame(10,0);   //Strike
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);  
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(112,game.calculateScore());
		
		
	}
	
	@Test
	public void testMultipleSparesFrame() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(8,2);  //Spare
		Frame frame2 = new Frame(5,5);   //Spare
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);  
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(98,game.calculateScore());
		
		
	}
	
	@Test
	public void testSpareAsTheLastFrame() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(1,5);  
		Frame frame2 = new Frame(3,6);   
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);  
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,8);   //Last frame is spare
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(90,game.calculateScore());
		
		
	}
	
	@Test
	public void testStrikeAsTheLastFrame() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(1,5);  
		Frame frame2 = new Frame(3,6);   
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);  
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(10,0);   //Last frame is strike
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(92,game.calculateScore());
		
		
	}
	
	@Test
	public void testBestScore() throws BowlingException  {
		
		Game game = new Game();
		
		Frame frame1 = new Frame(10,0);  
		Frame frame2 = new Frame(10,0);   
		Frame frame3 = new Frame(10,0);
		Frame frame4 = new Frame(10,0);
		Frame frame5 = new Frame(10,0);  
		Frame frame6 = new Frame(10,0);
		Frame frame7 = new Frame(10,0);
		Frame frame8 = new Frame(10,0);
		Frame frame9 = new Frame(10,0);
		Frame frame10 = new Frame(10,0);  
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(300,game.calculateScore());
		
		
	}

}
