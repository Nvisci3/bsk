package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import bsk.Frame;


public class FrameTest {

	@Test
	public void testFirstThrow() throws Exception{
		int inputFirstThrow = 2;
		int inputSecondThrow = 4;
		Frame frame = new Frame(inputFirstThrow,inputSecondThrow);
		assertEquals(2,frame.getFirstThrow());
	}
	
	@Test
	public void testSecondThrow() throws Exception{
		int inputFirstThrow = 2;
		int inputSecondThrow = 4;
		Frame frame = new Frame(inputFirstThrow,inputSecondThrow);
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test
	public void testFrameScore() throws Exception{
		int inputFirstThrow = 2;
		int inputSecondThrow = 6;
		Frame frame = new Frame(inputFirstThrow,inputSecondThrow);
		assertEquals(8,frame.getScore());
	}

}
